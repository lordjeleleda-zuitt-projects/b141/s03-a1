SHOW DATABASES;

USE blog_db;

INSERT INTO users (email, password, datetime_created)
VALUES ("johnsmith@gmail.com", "passwordA", "2021-1-1 01:00:00"),
("juandelacruz@gmail.com", "passwordB", "2021-1-1 02:00:00"),
("janesmith@gmail.com", "passwordC", "2021-1-1 03:00:00"),
("mariadelacruz@gmail.com", "passwordD", "2021-1-1 04:00:00"),
("johndoe@gmail.com", "passwordE", "2021-1-1 05:00:00");

SELECT * FROM users;

INSERT INTO posts (title, content, datetime_posted, author_id)
VALUES ("First Code", "Hello World!", "2021-1-2 01:00:00", 1),
("Second Code", "Hello Earth!", "2021-1-2 02:00:00", 1),
("Third Code", "Welcome to Mars!", "2021-1-2 03:00:00", 2),
("Fourth Code", "Bye bye solar system!", "2021-1-2 04:00:00", 4);

SELECT * FROM posts;

SELECT * FROM posts WHERE author_id = 1;

SELECT email, datetime_created FROM users;

UPDATE posts 
SET content = "Hello to the people of the Earth!"
WHERE content = "Hello Earth!"
AND author_id = 1;

SELECT * FROM posts WHERE title = "Second Code";

DELETE FROM users
WHERE email = "johndoe@gmail.com"

SELECT * FROM users;



